import React from 'react';

export class Selectable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: props.initialSelected
        }
    }

    componentWillReceiveProps(next){
        this.setState({
            selected: next.initialSelected,
            rows: next.rows
        });
    }

    shouldComponentUpdate(nextProps, nextState){
        const { state } = this;

        if (nextState.selected !== state.selected){
            return true;
        }

        return nextState.rows !== state.rows;

    }

    render(){
        const { props } = this;
        console.log('Rendering');
        return (
            <table>
                <tbody>
                {props.rows.map((r, idx) => {
                    return (
                        <tr
                            key={idx}
                            style={(this.state.selected === idx)?{backgroundColor:'black', color:'white'}:null}
                            onClick={() => {
                                this.props.onSelect(idx);
                            }}
                        >
                            {Object.keys(r).map((key, idx) => {
                                return (
                                    <td
                                        key={idx}
                                    >
                                        {r[key]}
                                    </td>
                                );
                            })}
                        </tr>
                    );
                })}
                </tbody>
            </table>
        );
    }
}
