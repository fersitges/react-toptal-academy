import React from 'react';

export class AddNewForm extends React.Component {
    constructor(props) {
        super(props);
        this.state =  {
            name: '',
            lastName: ''
        };
    }

    handleClick(){
        this.props.onAdd({
            name: this.state.name,
            lastName: this.state.lastName
        });
    }

    handleNameChange(e){
        //debugger;
        this.setState({
            name: e.target.value//.toLowerCase()
        });
    }

    handleLastNameChange(e){
        //debugger;
        this.setState({
            lastName: e.target.value//.toLowerCase()
        });
    }

    clear(){
        this.setState({
            name: '',
            lastName: ''
        });
    }

    render(){
        const { state } = this;
        return (
            <div>
                <div>
                    <span>Name:</span>
                    <input type="text"
                           value={state.name}
                           onChange={this.handleNameChange.bind(this)}
                    />
                </div>
                <div>
                    <span>Last Name:</span>
                    <input type="text"
                           value={state.lastName}
                           onChange={this.handleLastNameChange.bind(this)}
                    />
                </div>
                <div>
                    <button onClick={this.handleClick.bind(this)}>Add</button>
                </div>
            </div>
        );
    }
}
