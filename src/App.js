import React from 'react';
import { Selectable } from './components/Selectable';
import { AddNewForm } from './components/AddNewForm';

let data = [
    { name: 'Alejandro', lastname: 'Hernández' },
    { name: 'Viktor', lastname: 'Zira' },
    { name: 'Eva', lastname: 'Vidal' }
];

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: data,
            selected: 0
        };
        this.select = this.select.bind(this);
    }


    select(idx) {
        this.setState({
            selected: idx
        });
    }

    add(person) {
        let data = this.state.data.slice();
        data.push(person);
        this.setState({
            data: data
        }, () => {
            this.form.clear();
        });
    }

    render(){
        return (
            <div>
                <Selectable
                    rows={this.state.data}
                    initialSelected={this.state.selected}
                    onSelect={this.select}
                />
                <AddNewForm
                    onAdd={this.add.bind(this)}
                    ref={(form) => {
                        this.form = form;
                    }}
                />
            </div>
        );
    }
}

export default App;